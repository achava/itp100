1. What function does Python use for accessing files? What arguments does it need? What does it return?
The function is called 'open().' The argument it takes is the name of the file to open, and returns something called a file handle.
2. What is a file handle? Explain how it relates to a file? Where is the data stored?
A file handle is the 'wrapper' that allows you to access the file's operations. The data is stored int he file.
3. What is '\n'?
\n means 'new line', indicating if a print contains a new line. This is only 1 character.
4. What does Dr. Chuck say is the most common way to treat a file when reading it?
When reading a file, it is most commonly treated as a sequence of lines, \n at the end of each.
5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead? Explain your answer.
Yes, as it removes the last character. Since '\n' is one character, it will be removed.
6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'. Compare these three patterns, providing examples of each.
You can use the read() function to print the characters in a file/string, use an if statement to search for 'From:' and print it (stripping the newline), or using 'in' to print lines with a certain word/string.
7. A new Python construct is introduced at the end of the second video to deal with the situation when the program attempts to open a file that isn't there. Describe this new construct and the two new keywords that pair to make it.
This construct takes input from the user, counts lines in the file, and prints "There were [count] subject lines in [fname]",and prints "File cannot be opened: [fname]" if the file does not exist, then quits. The program uses the keywords try and except. Try tells the computer to do something, while the except section of code is run if the computer cannot run the try. 
