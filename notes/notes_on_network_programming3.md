1. What is ASCII? What are its limitations and why do we need to move beyond it?
ASCII is the American Standard Code for Information Interchange, or the characters we use daily on the Internet. Its limitations are more complex characters, like other languages.
2. Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)?
ord().
3. Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?
This complex set is called Unicode.
4. Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.
Bytes are the unit of storage. When you buy a storage device (like a hard drive), the units are always in 1024 (after byte). 1024 Bytes make up a Kilo Byte, 1024 Kilo Bytes make up a Mega Byte, etc.
5. Break down the process of using .encode() and .decode() methods on data we send over a network connection.
When a string needs to be sent over a network connection, .encode() translates the Unicode into UTF-8. Likewise, if data is being received, .decode decodes the UTF-8 and spits out Unicode.
