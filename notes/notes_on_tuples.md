1. Dr. Chuck begins this lesson by stating that tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike?
Tuples are like lists in the way that they have indexed items which can be called.
2. How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?
Tuples are "immutable", meaning its contents can't be changed once assigned.
3. Dr. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?
Tuple assignment is the initial creation of a tuple, where essentially a read-only list is made. A few examples include using two variables to store two values in the same line of code or in dictionaries to easily pack data.
4. Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.
You can use tuples in a list or dictionary to make changes you can't on tuples by themselves, and dynamic lists will save even more time and energy in one line.
5. In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?
The example made sense and I understood the example printing out the sorted value,key list. 
