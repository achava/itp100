1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?
Conditional, sequencial, and store and reuse.
2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?
It starts a countdown from 5 to blastoff by reducing n by 1 every time the while loop goes around.
3. What is another word for looping introduced in the video?
An iteration
4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?
This is an infinite loop. If you write and run an infinite loop, it will keep going until your computer dies or you terminate it. This can be bad for your computer because of all the processing it's doing at once, and it can crash it.
5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?
"Break". What this does is exit the statement and run the code below it. Dr. Chuck uses it to allow the user to terminate the program when they type 'done'.
6. What is the other loop control statement introduced in the first video? What does it do?
"Continue". This restarts the loop. Dr. Chuck uses it to prevent the program from printing when the user is making a comment with '#'.
7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?
A while loop is an indefinite loop, the next loop discussed is a definite loop.
8. Which new Python loop is introduced in the second video? How does it work?
The next Python loop is a definite loop, which has a set amount of repititions from a list of items. While indefinite loops only check for a condition, definite loops use the input to accomplish something, then move on.
9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?
Loop idioms are patterns used within loops to make them "smarter". The idioms Dr. Chuck shows are 2 that outputs the greatest number from a string of numbers, and another that outputs the smallest number from a string of numbers.
