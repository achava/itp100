1. Dr. Chuck mentions the architecture that runs our network. What is the name of this architecture?
The TCP architecture, which makes sure that all data is delivered properly.
2. Take a close look at the slide labelled Stack Connections. Which layer does Dr. Chuck say we will be looking at, assuming that one end of this layer can make a phone call to the corresponding layer on the other end? What are the names of the two lower level layers that we will be abstracting away?
The transport layer is the layer where "phone calls" can be made. The two lower layers to focus on later are the Internet and link layer. 
3. We will be assuming that there is a ____________ that goes from point A to point B. There is a ______________ running at each end of the connection. Fill in the blanks.
Pipe, Program.
4. Define Internet socket as discussed in the video.
If there is a pipe that runs on both sides of the Internet, the processes running on either side using it are the sockets (its endpoint).
5. Define TCP port as discussed in the video.
Since this 'pipe' has several endpoints on either side, there are several processes managing each one. Each process is assigned a TCP port (which is for the most part universal, so a user knows which to access immediately) 
6. At which network layer do sockets exist?
Sockets exist on the transport layer.
7. Which network protocol is used by the World Wide Web? At which network layer does it operate?
The World Wide Web uses HTTP (HyperText Transfer Protocol) to transmit data, and operates on the application level.
