# Lesson 5 Notes

1. How can we use Markdown and git to create nice looking documents under revision control?
	Markdown, like Docs, can be saved and edited later, keeping the changes and a log.
2. Dr. Chuck describes functions as store and reused pattern in programming. Explain what he means by this. (Note: I will present another view of functions, as a tool for allowing procedural abstraction, in our class discussion, and will emphasize just what an important idea that is).
	What Dr. Chuck means by this is that functions are essentially programs within programs. 
3. How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python.
	We create functions in Python by putting a keyword (anything not a reserved word), its parameters and commands to be executed.
ex - def printVar
	print(x)
	x = x + 1
4. Dr. Chuck shows that nothing is output when you define a function, what he calls the store phase. What does he call the process that makes the function run? He uses two words for this, and it is really important to understand this idea and learn the words for it.
	Dr. Chuck calls it "call and reuse". The function is called, the program searches for its definition, and runs the code.
5. Provide some examples of built-in function in Python.
	Some examples of built-in functions are print(), input() and type(), where calling it prompts Python to search for those lines of code and run them.
