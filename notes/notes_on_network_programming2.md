1. What is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?
An application protocol is a protocol that dictates use and data transfer on the application layer. Some examples are Telnet and HTTP/S. Another protocol we use every day is DNS, which lets us access BigBlueButton. 
2. Name the three parts of a URL. What does each part specify?
The 3 basic parts of a URL are the protocol (which dictates how data to the domain will be sent), the domain (specifying the website and application), and the path (specifying the page of the domain).
3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works?
The request/reponse cycle is the process of "calling" a server, and getting a response from it. Dr. CHuck calls the socket used on either side a "phone", and the data transferred is a "conversation"
4. What is the IETF? What is an RFC?
The IETF is the Internet Engineering Task Force, and they develop Internet standards. An RFC is a Request For Comments, where the IETF will publish development/standards for all to see.
5. In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples?
We find the files in Python For Everyone, Materials, and in a ZIP file called Sample Code. In that folder are all the codes from the lectures.
6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet? (NOTE: examples in the previous videos no longer work. I suspect this is because HTTP/1.0 is no longer supported by the webserver).
I am working on making it work, but so far haven't made progress.

