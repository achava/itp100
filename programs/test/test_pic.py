import pygame

pygame.init()

run = True

display_width = 800
display_height = 600

gameDisplay = pygame.display.set_mode((display_width,display_height))
clock = pygame.time.Clock()

carImg = pygame.image.load('racecar.png')
gameDisplay.fill(255,255,255)
gameDisplay.blit(carImg, (0,0))

while run == True:

    # completely fill the surface object
    # with white colour

    # copying the image surface object
    # to the display surface object at
    # (0, 0) coordinate.

    # iterate over the list of Event objects
    # that was returned by pygame.event.get() method.
    for event in pygame.event.get() :

        # if event object type is QUIT
        # then quitting the pygame
        # and program both.
        if event.type == pygame.QUIT :

            # deactivates the pygame library
            pygame.quit()
#program failed
