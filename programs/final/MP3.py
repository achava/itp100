import pygame
import os

pygame.init()

display_width = 1000
display_height = 1000

black = (0,0,0)
white = (255,255,255)
light_blue = (161, 221, 223)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)

done = False

gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('Banana Music')
clock = pygame.time.Clock()
clock.tick(60)

#button_size = (50,50)
#playB = pygame.transform.scale(pygame.image.load(os.path.join("assets",'play_button.png')),button_size)
#pauseB = pygame.transform.scale(pygame.image.load(os.path.join("assets",'pause_button.png')),button_size)
#backwardsB = pygame.transform.scale(pygame.image.load(os.path.join("assets",'backwards_button.png')),button_size)
#skipB = pygame.transform.scale(pygame.image.load(os.path.join("assets",'skip_button.png')),button_size)

def shuffle():
    pygame.mixer.init()
    pygame.mixer.music.load((os.path.join("assets","Title - The Legend of Zelda Link's Awakening (2019) Soundtrack")))
    pygame.mixer.music.play()
#def stop():
#def listen():

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                shuffle()
            elif event.key == pygame.K_o:
                #stop()
                print("stop")
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_p or event.key == pygame.K_o:
                #listen()            
                print("listen")
    gameDisplay.fill(light_blue)
    print(event) 
    pygame.display.update()
    clock.tick(60)

#gameDisplay.blit(backwardsB,(x,y))

#gameDisplay.blit(playB,(x1,y1))

#gameDisplay.blit(skipB,(x2,y2))

#   gameDisplay.blit(pauseB,(x1,y1))



pygame.quit()
quit()
