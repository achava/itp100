def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
