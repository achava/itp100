def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]

def primary(election, A, B):
    """
    Return the primary winner of the election with round 1 candidates A and B. 
    Complete a primary between A and B, then complete a primary between the winner of A/B and C.
 
      >>> primary([(1, 2, 3), (2, 3, 1), (2, 1, 3)], 1, 3)
      2
      >>> primary([(1, 2, 3), (2, 3, 1), (2, 1, 3)], 2, 3)
      2
      >>> primary([(3, 2, 1), (3, 2, 1), (2, 1, 3)], 1, 2)
      3
    """
    # Complete a primary between A and B
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] not in [A, B]:
            vote_totals[vote[1]] += 1      

    # Record the candidate number with the highest vote count
    winner =  max(vote_totals.items(), key=lambda x: x[1])[0]

    # Determine Finalist
    lis  = [1, 2, 3]
    lis.remove(A)
    lis.remove(B)
    C = lis[0]

    # Complete second primary between winner and finalist
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] not in [winner, C]:
            vote_totals[vote[1]] += 1   
        else: 
            vote_totals[vote[0]] += 1   

    return max(vote_totals.items(), key=lambda x: x[1])[0]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
